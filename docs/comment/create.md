# Комментарий

#### Создание ком ментария

> **Url**: api/comment/create

> **Method**: POST

### Требуемые поля

```
"article_id" //   id статьи
"text" //   текст комментария

```
### Пример

```
{
	"article_id" : "5",
    'text': "Я согласен, овощи весьма полезны",
}

```

##### Ответ

 
>code : 200

```
{
    "status": "success",
    "message": "Comment sent for moderation!"
}
```