# Статья

#### Обновление статьи

> **Url**: api/article/update

> **Method**: PUT

### Требуемые поля

```
'id' // id статьи
"name" //   название статьи
"text" //   текст статьи
"preview" // превью
"category" // id категории

```
### Пример

```
{
	"id": "5"
	"name" : "Овощи",
    'text': "Овощи вредны для здоровья",
	"preview" : d2b8705e7f07c4acaf5aea97aad3b039.jpg,
	"category": 1
}

```

##### Ответ

 
>code : 200

```
{
    "status": "success",
    "message" => "Article updated successfully!"

}
```