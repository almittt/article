# Статья

#### Удаление статьи

> **Url**: api/article/delete

> **Method**: DELETE

### Требуемые поля

```
'id' // id статьи
```
### Пример

```
{
	"id": "2"
}

```

##### Ответ

 
>code : 200

```
{
    "status": "success",
    "message" => "Article successfully deleted!"

}
```