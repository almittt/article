# Статья

#### Создание статьи

> **Url**: api/article/create

> **Method**: POST

### Требуемые поля

```
"name" //   название статьи
"text" //   текст статьи
"preview" // превью
"category" // id категории

```
### Пример

```
{
	"name" : "О пользе овощей...",
    'text': "Польза  нереальнааая!!!",
	"preview" : d2b8705e7f07c4acaf5aea97aad3b039.jpg,
	"category": 1
}

```

##### Ответ

 
>code : 200

```
{
    "status": "success",
    "message" => "Article successfully created!"

}
```