# Статья

#### Отобразить выбранную  статью

> **Url**: api/article/show/{id}

> **Method**:  GET

### Требуемые поля

```
'id' // id статьи

```
### Пример

```
{
	"id": "3"
}

```

##### Ответ

 
>code : 200

```

{
	"status": "success"
    "id": 3,
    "name": "Бокс",
    "text": "Держи удар, хлюпик!",
    "preview": "/var/www/public/1580917489.jpg",
    "rating": 1,
    "category": "Здоровье и спорт",
    "user_name": "Андрей",
    "user_surname": "Новик"
}

```