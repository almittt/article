# Статья

#### Просмотр статей авторизованного пользователя

> **Url**: api/article/my

> **Method**: GET 

##### Ответ

code : 200
```
{
    "articles": [
        {
            "id": 5,
            "name": "О пользе овощей...",
            "text": "Польза  нереальнааая!!!",
            "preview": "/1581318463.jpg",
            "category_id": 1,
            "user_id": 1,
            "rating": 0,
            "created_at": "2020-02-10 07:07:43",
            "updated_at": "2020-02-10 07:07:43",
            "status": "moderated"
        },
        {
            "id": 3,
            "name": "Бокс",
            "text": "Держи удар, хлюпик!",
            "preview": "/1580917489.jpg",
            "category_id": 2,
            "user_id": 1,
            "rating": 1,
            "created_at": "2020-02-05 15:44:49",
            "updated_at": "2020-02-10 07:30:05",
            "status": "active"
        }
    ]
}