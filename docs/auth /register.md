# Регистрация

#### Регистрация
> **Url**: api/auth/registration 

> **Method**: POST


Для регистраций пользователя требуется следующие поля:

```
"name" //   имя пользователя
"surname" //   фамилия пользователя
"role" // Поле role должен содержать один из следующих значений
        "author" // для автора
        "user" // для читателя
"login" // логин
"email" // email
"password" // пароль, строка, должен содержать как минимум 8 символов
"password_confirmation" // подтверждение пароля

```
##### Пример:

```
{
	"name" : "Александр",
    'surname': "Свистоплясов"
	"email" : "svist@mail.ru",
    "login": "svistok"
	"password" : "qwertyqwerty",
    "password_confirmation" : "qwertyqwerty",
	"role" : user
}
```


##### Ответ

code : 200
```
{
    "status": "success",
    "message" => "Successfully registration!""

}
```

Если код ошибки 422 то произошло ошибка валидаций некоторых полей. Ошибочные поля указывается в поле "errors" в 
#####  Пример ошибки
```
{
    "status": "error",
    "errors": {
        "login": [
            "The login has already been taken."
        ]
    }
}
```



