## Авторизация

Для аутентификаций пользователя используется email и пароль. После аутентификаций генерируется JWT токен, который используется для авторизаций.
Авторизация пользователя происходит с использованием JWT токена и тип авторизаций, например Bearer.


##### Аутентификация

> **Url**: api/auth/login

> **Method**: POST

Для аутентификаций пользователя требуется следующие поля:

```
"email" // email пользователя указанный при регистраций
"password" // пароль
```



###### Пример:

Url: http://article:8081/api/auth/login

###### Запрос

```
{
	"email":"novik@mail.ru",
	"password" : "qwertyui"
}
```

###### Ответ

При успешном аутентификаций будет возвращен токен авторизаций.
```
"status"         // статус ответа "success"
"access_token"   // JWT токен для авторизаций
"token_type" // Тип токкена для авторизаций, всегда будет "Bearer"
"expires_in" // Количество секунд, за которые действует токен
```


> code : 200
```
{
    "status": "success",
    "access_token": "eyJ0eXAiOiJKV1QiL...",
    "token_type": "bearer",
    "expires_in": 3600
  
```

Если код ошибки 422 то произошло ошибка валидаций некоторых полей. Ошибочные поля указывается в поле "errors" в ответе

###### Пример ошибки

code : 422
```
{
    "message": "The given data was invalid.",
    "errors": {
        "email": [
            "The email must be a valid email address."
        ]
    }
}
```

Код 401 если пользователь не зарегистрирован

###### Пример ошибки

code : 401
```
{
    "error": "Unauthorized"
}
```


Для операций требующих авторизованного пользователя нужно использовать полученный токен. Токен передается в заголовке HTTP запроса.

```
Authorization: Bearer eyJ0eXAiOiJ...
```

## Обновление токена

> **Url**: api/auth/refresh

> **Method**: GET

Обновления токена требуется для того, чтобы при истекшем времени токена получить новый, чтобы продолжать работу.
Чтобы получить обновленный токен нужно в заголовке передать старый токен, который получили при авторизаций или при предыдущем обновлений.
```
Authorization: Bearer eyJ0eXAiOiJ...
```

И в ответ получите новый токен.

###### Ответ

> code : 200
```
{
    "status": "success",
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcnRpY2xlOjgwODFcL2FwaVwvYXV0aFwvcmVmcmVzaCIsImlhdCI6MTU4MTMxNDQwMiwiZXhwIjoxNTgxMzE4MDY0LCJuYmYiOjE1ODEzMTQ0NjQsImp0aSI6Indva3dvTkNhNkhsR0VHV3IiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.kV-XjiuEwmUJ-Ghe7DJPKzI0oQgHAixyRGqIXp4wsnc",
    "token_type": "bearer",
    "expires_in": 3600
}
```


## Текущий пользователь
> **Url**: api/auth/me

> **Method**: GET

Способ получить текущего авторизованного пользователя.

Авторизация обязательно. Нужно передать токен в заголовке.

##### Пример

> code : 200
```
{
    "id": 1,
    "name": "Андрей",
    "email": "novik@mail.ru",
    "email_verified_at": null,
    "created_at": "2020-01-28 09:51:59",
    "updated_at": "2020-02-09 13:45:43",
    "login": "novikov",
    "role": "author",
    "surname": "Новиков",
    "status": "active"
}
``` 