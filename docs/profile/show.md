# Профиль

#### Просмотр профиля

> **Url**: api/profile/show

> **Method**: GET 

##### Ответ

code : 200
```
{
    "id": 1,
    "name": "Андрей",
    "surname": "Новиков",
    "role": "author"
}