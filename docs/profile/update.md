# Профиль

#### Обновление профиля

> **Url**: api/profile/show

> **Method**: GET

### Требуемые поля

```
"name" //   имя пользователя
"surname" //   фамилия пользователя
"role" // Поле role должен содержать один из следующих значений
        "author" // для автора
        "user" // для читателя
``` 

##### Пример:

```
{
	"name" : "Андрей",
    'surname': "Новик"
	"role" : author
}

``` 


##### Ответ

code : 200
```
{
    "status": "success",
    "message" => "Profile updated!"

}
```