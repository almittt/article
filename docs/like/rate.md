# Лайки

#### Оценка выбранной статьи

> **Url**: api/comment/create

> **Method**: PUT

### Требуемые поля

```
"article_id" //   id статьи

```
### Пример

```
{
	"article_id" : "5",
}

```

##### Ответ

 
>code : 200

```
{
    "status": "success",
    "message": "Article successfully liked!"
}
```