@extends('layouts.app', ['page' => __('Статьи'), 'pageSlug' => 'articles'])

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
					@include('alerts.success')                	
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ $article->name }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('article.index') }}" class="btn btn-sm btn-primary">{{ __('Вернуться к списку') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
						<p>
						  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						    Текст статьи 
						  </a>                 
						</p>
						<div class="collapse" id="collapseExample">
						  <div class="card card-body">
		                    {{ $article->text }}
						  </div>
						</div>
						<div class="row">
							<div class="col-md-6">
			                    <form method="post" action="{{route('article.active', ['id' => $article->id])}}">
			                    	@csrf
			                    	@method('put')
								    <div class="form-group">
								        <label for="note">Замечания:</label>
								        <textarea class="form-control" id="note" name="note" rows="3">
								        </textarea>
						                <button type="submit" class="btn btn-success">Принять</button>
								    </div>
								</form>
							</div>
							<div class="col-md-6">
								<form method="post" action="{{route('article.inactive', ['id' => $article->id])}}">
			                    	@csrf
			                    	@method('put')
								    <div class="form-group">
								        <label for="reason">Причина отказа:</label>
								        <textarea class="form-control" id="reason" name="reason" rows="3">
								        </textarea>
				   		                <button type="submit" class="btn btn-danger">Отклонить</button>
								    </div>
								</form>							
							</div>
						</div>
                    </div>
               </div>
            </div>
	        <div class="col-md-4">
	            <div class="card card-user">
	                <div class="card-body">
	                    <p class="card-text">
	                        <div class="author">
	                            <div class="block block-one"></div>
	                            <div class="block block-two"></div>
	                            <div class="block block-three"></div>
	                            <div class="block block-four"></div>
	                            <a href="#">
	                                <img class="avatar" src="{{ asset('images')}}{{$article->preview}}" alt="article_preview">
	                                <h5 class="title">
	                                	{{ $article->user->name }} {{ $article->user->surname }}
	                                </h5>
	                            </a>
	                            <p class="description">
            	                   	{{$article->category->name}}
	                            </p>
								<button type="button" class="btn btn-primary">
									Рейтинг <span class="badge badge-default">{{$article->rating}}</span>
								</button>
								@if(auth()->user()->role === 'admin')
								<form method="post" action="{{route('article.rate.reset', [$article->id])}}">
                                      @csrf
                                      @method('put')
									  <button type="submit" rel="tooltip" class="btn btn-danger btn-sm btn-icon">
									        <i class="tim-icons icon-refresh-02"></i>
									  </button>
								</form>
								@endif	
	                        </div>
	                    </p>
	                </div>
	            </div>
	        </div>
        </div>
    </div>
@endsection
