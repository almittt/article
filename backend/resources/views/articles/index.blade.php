@extends('layouts.app', ['page' => __('Управление статьями'), 'pageSlug' => 'articles'])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">{{ __('Статьи') }}</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('alerts.success')
                    <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Фильтр
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a 
                              class="dropdown-item" href="{{ route('article.index', ['category' => null]) }}">Все категории 
                          </a>
                          @foreach ($categories as $category => $value )
                            <a 
                                class="dropdown-item" href="{{ route('article.index', ['category' => $value->id]) }}">{{ $value->name }}
                            </a>
                          @endforeach
                      </div>
                    </div>
                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <th scope="col">{{ __('Название') }}</th>
                                <th scope="col">{{ __('Категория') }}</th>
                                <th scope="col">{{ __('Автор') }}</th>
                                <th scope="col">{{ __('Рейтинг') }}</th>
                                <th scope="col">{{ __('Дата создания') }}</th>
                                <th scope="col">{{ __('Статус') }}</th>
                                <th scope="col"></th>
                            </thead>
                            <tbody>
                                @foreach ($articles as $article)
                                    <tr>
                                        <td>{{ $article->name }}</td>
                                        <td>{{ $article->category->name }}</td>
                                        <td>{{ $article->user->name }} {{ $article->user->surname }}</td>
                                        <td>{{ $article->rating }}</td>
                                        <td>{{ $article->created_at->format('d.m.Y H:i') }}</td>
                                        <td>{{ $article->status }}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    @if (auth()->user()->role === 'admin' )
                                                        <a class="dropdown-item" href="{{ route('article.show', ['article' => $article->id]) }}">{{ __('Просмотр') }}</a>
                                                        <form
                                                         action="{{
                                                          route('article.destroy',
                                                           ['id' => $article->id])
                                                            }}" method="post">
                                                            @csrf
                                                            @method('delete')

                                                            <button type="button" class="dropdown-item" onclick="confirm('{{ __("Вы уверены, что хотите удалить статью?") }}') ? this.parentElement.submit() : ''">
                                                                        {{ __('Удалить') }}
                                                            </button>
                                                        </form>
                                                    @else
                                                        <a class="dropdown-item" href="{{ route('article.show', ['article' => $article->id]) }}">{{ __('Просмотр') }}</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
