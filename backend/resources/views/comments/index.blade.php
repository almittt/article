@extends('layouts.app', ['page' => __('Управление комментариями'), 'pageSlug' => 'comment'])

@section('content')
	<div class="row">
		<div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">{{ __('Комментарии') }}</h4>
                        </div>
                    </div>
                </div>
				<table class="table">
				    <thead>
				        <tr>
				            <th>Дата создания</th>
				            <th>Текст</th>
				            <th>Статья</th>
				            <th>Пользователь</th>
				            <th>Статус</th>
				            <th class="text-right">Действия</th>
				        </tr>
				    </thead>
				    <tbody>
				    	@foreach($comments as $comment)
					        <tr>
					            <td>{{ $comment->created_at->format('d.m.Y H:i') }}</td>
					            <td>{{$comment->text}}</td>
					            <td>{{$comment->article->name}}</td>
					            <td>{{$comment->user->login}}</td>
					            <td>{{$comment->status}}</td>
					            <td class="td-actions text-center">
					                <a
					                	href="{{route('comment.show', ['id' => $comment->id]) }}" type="button"
					                	rel="tooltip"
					                	class="btn btn-primary btn-sm btn-icon">
					                    <i class="tim-icons icon-alert-circle-exc"></i>
					                </a>
					            </td>
					            {{--<td class="td-actions text-right">
					                <a href="{{route('category.edit', ['id' => $category->id]) }}" type="button" rel="tooltip" class="btn btn-success btn-sm btn-icon">
					                    <i class="tim-icons icon-settings"></i>
					                </a>
					                <a type="button" rel="tooltip" class="btn btn-danger btn-sm btn-icon">
					                    <i class="tim-icons icon-simple-remove"></i>
					                </a>
					            </td>--}}
					        </tr>
				        @endforeach
				    </tbody>
				</table>
            </div>
		</div>
	</div>
@endsection

