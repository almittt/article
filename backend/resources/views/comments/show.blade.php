@extends('layouts.app', ['page' => __('Комментарии'), 'pageSlug' => 'comments'])

@section('content')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ $comment->article->name }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('article.index') }}" class="btn btn-sm btn-primary">{{ __('Вернуться к списку') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
						<p>
						  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						    Текст комментария 
						  </a>                 
						</p>
						<div class="collapse" id="collapseExample">
						  <div class="card card-body">
		                    {{ $comment->text }}
						  </div>
						</div>
						<div class="row">
							<div class="col-md-6">
			                    <form method="post" action="{{route('comment.accept', ['id' => $comment->id])}}">
			                    	@csrf
			                    	@method('put')
								    <div class="form-group">
								        <label for="note">Замечания:</label>
								        <textarea class="form-control" id="note" name="note" rows="3">
								        	
								        </textarea>
						                <button href="{{route('comment.accept', ['id' => $comment->id])}}" class="btn btn-success">Принять</button>
								    </div>
								</form>
							</div>
							<div class="col-md-6">
								<form method="post" action="{{route('comment.reject', ['id' => $comment->id])}}">
			                    	@csrf
			                    	@method('put')
								    <div class="form-group">
								        <label for="reason">Причина отказа:</label>
								        <textarea class="form-control" id="reason" name="reason" rows="3">
								        	
								        </textarea>
				   		                <button href="{{route('comment.reject', ['id' => $comment->id])}}" class="btn btn-danger">Отклонить</button>
								    </div>
								</form>							
							</div>
						</div>
                    </div>
               </div>
            </div>
	        <div class="col-md-4">
		            <div class="card card-user">
		                <div class="card-body">
		                    <p class="card-text">
		                        <div class="author">
		                            <div class="block block-one"></div>
		                            <div class="block block-two"></div>
		                            <div class="block block-three"></div>
		                            <div class="block block-four"></div>
		                            <a href="#">
		                                <img class="avatar" src="{{asset('images')}}{{$comment->article->preview}}" alt="article_preview">
		                                <h5 class="title">
		                                	{{ $comment->user->name }} {{ $comment->user->surname }}
		                                </h5>
		                            </a>
		                        </div>
		                    </p>
		                </div>
		            </div>
	        </div>
        </div>
    </div>
@endsection
