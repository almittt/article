@extends('layouts.app', ['page' => __('Управление категориями'), 'pageSlug' => 'category'])

@section('content')
	<div class="row">
		<div class="col-md-12">
            <div class="card ">
                @include('alerts.success')
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">{{ __('Категории') }}</h4>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{route('category.create')}}" class="btn btn-sm btn-primary">{{ __('Добавить') }}</a>
                        </div>
                    </div>
                </div>
				<table class="table">
				    <thead>
				        <tr>
				            <th>Наименование</th>
				            <th>Дата создания</th>
				            <th class="text-right">Действия</th>
				        </tr>
				    </thead>
				    <tbody>
				    	@foreach($categories as $category)
					        <tr>
					            <td>{{$category->name}}</td>
					            <td>{{ $category->created_at->format('d.m.Y H:i') }}</td>
					            <td class="td-actions text-right">
					                <a href="{{route('category.edit', ['id' => $category->id]) }}"  rel="tooltip" class="btn btn-success btn-sm btn-icon">
					                    <i class="tim-icons icon-settings"></i>
					                </a>
					                <form method="post" action="{{route('category.destroy', ['id' => $category->id]) }}">
					                    @csrf
					                    @method('delete')
						                <button type="submit" rel="tooltip" class="btn btn-danger btn-sm btn-icon">
						                    <i class="tim-icons icon-simple-remove"></i>
						                </button>
					                </form>
					            </td>
					        </tr>
				        @endforeach
				    </tbody>
				</table>
            </div>
		</div>
	</div>
@endsection

