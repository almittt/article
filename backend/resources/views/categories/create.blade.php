@extends('layouts.app', ['page' => __('Управление категориями'), 'pageSlug' => 'category'])

@section('content')
	<div class="card">
	  <div class="card-body">
	    <form method="POST" action="{{route('category.store')}}">
	      @csrf
	      @method('post')            
	      <div class="form-group">
	        <label for="name">Наименование</label>
	        <input type="text" class="form-control" name="name" id="name" placeholder="Наименование">
	      </div>
	      <button type="submit" class="btn btn-primary">Добавить</button>
	    </form>
	  </div>
	</div>
@endsection('content')

