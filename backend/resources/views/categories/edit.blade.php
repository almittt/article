@extends('layouts.app', ['page' => __('Управление категориями'), 'pageSlug' => 'category'])

@section('content')
	<div class="card">
	  <div class="card-body">
	    <form method="POST" action="{{route('category.update', ['id' => $category->id])}}">
	      @csrf
	      @method('put')            
	      <div class="form-group">
	        <label for="name">Наименование</label>
	        <input type="text" class="form-control" name="name" id="name" placeholder="Наименование" value="{{$category->name}}">
	      </div>
	      <button type="submit" class="btn btn-primary">Обновить</button>
	    </form>
	  </div>
	</div>
@endsection('content')

