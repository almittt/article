@extends('layouts.app', ['page' => __('Управление пользователями'), 'pageSlug' => 'users'])

@section('content')
  <div class="card">
    <div class="card-body">
      <h3>Статус: {{$user->status}}</h3>
      <form>
        <div class="form-group">
          <label for="name">Имя</label>
          <input disabled="true" type="text" class="form-control" id="name" value="{{$user->name}}" placeholder="Имя" name="name" >
        </div>
        <div class="form-group">
          <label for="surname">Фамилия</label>
          <input disabled="true" type="text" class="form-control" id="surname" value="{{$user->surname}}" placeholder="Фамилия" name="surname" >
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input disabled="true" type="email" class="form-control" id="email" placeholder="Email" value="{{$user->email}}" name="email">  
        </div>
      </form>
      <div class="row">
        <div class="col-md-6">
          <form method="post" action="{{route('user.active', ['id' => $user->id])}}">
            @csrf
            @method('put')
              <div class="form-group">
                  <label for="note">Пожелания:</label>
                  <textarea class="form-control" id="note" name="note" rows="3">
                    
                  </textarea>
                  <button type="submit" class="btn btn-success">Принять</button>
              </div>
          </form>
        </div>
        <div class="col-md-6">
          <form method="post" action="{{route('user.inactive', ['id' => $user->id])}}">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="reason">Причина бана:</label>
                <textarea class="form-control" id="reason" name="reason" rows="3">
                  
                </textarea>
                <button type="submit" class="btn btn-danger">Отклонить</button>
            </div>
          </form>             
        </div>
      </div>
    </div>
  </div>
@endsection