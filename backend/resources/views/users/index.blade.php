@extends('layouts.app', ['page' => __('Управление пользователями'), 'pageSlug' => 'users'])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">{{ __('Пользователи') }}</h4>
                        </div>
                        @if(auth()->user()->role === 'admin')
                        <div class="col-4 text-right">
                            <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">{{ __('Добавить модератора') }}</a>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    @include('alerts.success')
                    <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Фильтр
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a 
                              class="dropdown-item" href="{{ route('user.index', ['role' => null]) }}">Все участники регаты  
                          </a>
                          @foreach ($roles as $role => $value )
                            <a 
                                class="dropdown-item" href="{{ route('user.index', ['role' => $role]) }}">{{ $value }}
                            </a>
                          @endforeach
                      </div>
                    </div>
                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <th scope="col">{{ __('Имя') }}</th>
                                <th scope="col">{{ __('Фамилия') }}</th>
                                <th scope="col">{{ __('Логин') }}</th>
                                <th scope="col">{{ __('Email') }}</th>
                                <th scope="col">{{ __('Дата создания') }}</th>
                                <th scope="col">{{ __('Статус') }}</th>
                                <th scope="col"></th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->surname }}</td>
                                        <td>{{ $user->login }}</td>
                                        <td>
                                            <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                        </td>
                                        <td>{{ $user->created_at->format('d.m.Y H:i') }}</td>
                                        <td>{{ $user->status }}</td>
                                        <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        @if (auth()->user()->role === 'admin' )
                                                            <a class="dropdown-item" href="{{ route('user.show', ['id' => $user->id] ) }}">{{ __('Просмотр.') }}</a>
                                                            <a class="dropdown-item" href="{{ route('comment.index', ['user' => $user->id])}}">{{ __('Комментарии') }}</a>
                                                            <form action="{{ route('user.destroy', $user) }}" method="post">
                                                                @csrf
                                                                @method('delete')

                                                                <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                                                                            {{ __('Удалить') }}
                                                                </button>
                                                            </form>
                                                        @else
                                                            <a class="dropdown-item" href="{{ route('user.show', ['id' => $user->id] ) }}">{{ __('Просмотр.') }}</a>
                                                            <a class="dropdown-item" href="{{ route('comment.index', ['user' => $user->id])}}">{{ __('Комментарии') }}</a>
                                                        @endif
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        {{ $users->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
