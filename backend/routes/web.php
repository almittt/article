<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth', 'role']);

Route::group(['middleware' => ['auth', 'role']], function () {
		Route::get('icons', ['as' => 'pages.icons', 'uses' => 'PageController@icons']);
		Route::get('maps', ['as' => 'pages.maps', 'uses' => 'PageController@maps']);
		Route::get('notifications', ['as' => 'pages.notifications', 'uses' => 'PageController@notifications']);
		Route::get('rtl', ['as' => 'pages.rtl', 'uses' => 'PageController@rtl']);
		Route::get('tables', ['as' => 'pages.tables', 'uses' => 'PageController@tables']);
		Route::get('typography', ['as' => 'pages.typography', 'uses' => 'PageController@typography']);
		Route::get('upgrade', ['as' => 'pages.upgrade', 'uses' => 'PageController@upgrade']);
});

Route::group(['middleware' => ['auth', 'role'], 'namespace' => 'Admin'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('user/show/{id}', 'UserController@show')->name('user.show');
	Route::put('user/active/{id}', 'UserController@active')->name('user.active');
	Route::put('user/inactive/{id}', 'UserController@inactive')->name('user.inactive');
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group([
    'namespace' => 'Admin',
	'middleware' => ['auth', 'role']    
], function () {
	Route::get('article', 'ArticleController@index')->name('article.index');
	Route::get('article/show', 'ArticleController@show')->name('article.show');
	Route::put('article/active/{id}', 'ArticleController@active')->name('article.active');
	Route::put('article/inactive/{id}', 'ArticleController@inactive')->name('article.inactive');
	Route::put('article/rate/reset/{id}', 'ArticleController@reset')->name('article.rate.reset');
	Route::delete('article/delete/{id}', 'ArticleController@destroy')->name('article.destroy');
});

Route::group([
	'middleware' => ['auth', 'role'],
    'namespace' => 'Admin'
], function () {
	Route::get('comment', 'CommentController@index')->name('comment.index');
	Route::get('comment/show/{id}', 'CommentController@show')->name('comment.show');
	Route::put('comment/accept/{id}', 'CommentController@accept')->name('comment.accept');
	Route::put('comment/reject/{id}', 'CommentController@reject')->name('comment.reject');
});

Route::group([
	'middleware' => ['auth', 'role'],
    'namespace' => 'Admin'
], function () {
	Route::get('category', 'CategoryController@index')->name('category.index');
	Route::get('category/create', 'CategoryController@create')->name('category.create');
	Route::get('category/edit/{id}', 'CategoryController@edit')->name('category.edit');
	Route::post('category/store', 'CategoryController@store')->name('category.store');
	Route::put('category/update/{id}', 'CategoryController@update')->name('category.update');
	Route::delete('category/delete/{id}', 'CategoryController@destroy')->name('category.destroy');
});
