<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'as' => 'auth.',
    'namespace' => 'Api\Auth',
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Controller@login');
    Route::post('registration', 'Controller@registration');
    Route::get('logout', 'Controller@logout')->middleware('jwt.verify');
    Route::get('refresh', 'Controller@refresh')->middleware('jwt.verify');
    Route::get('me', 'Controller@me')->middleware('jwt.verify');
});
 

Route::group([
    'as' => 'profile.',
    'namespace' => 'Api\Profile',
    'prefix' => 'profile',
    'middleware' => 'jwt.verify'

], function () {
    Route::get('show', 'Controller@show');
    Route::put('update', 'Controller@update')->middleware('user.status');
});

Route::group([
    'as' => 'article.',
    'namespace' => 'Api\Article',
    'prefix' => 'article',
	'middleware' => 'jwt.verify'    
], function () {
	Route::get('my', 'Controller@my');	
    Route::get('show/{id}', 'Controller@show')->middleware('article.status');
    Route::post('create', 'Controller@create')->middleware('user.status');
    Route::put('update', 'Controller@update')->middleware('user.status');
    Route::put('rate', 'Controller@rate')->middleware('user.status');
    Route::delete('delete', 'Controller@delete')->middleware('user.status');
});

Route::group([
    'as' => 'comment.',
    'namespace' => 'Api\Comment',
    'prefix' => 'comment',
	'middleware' => 'jwt.verify'    
], function () {
    Route::get('show/{id}', 'Controller@show')->middleware('article.status'); /*middleware('comment.status');*/
    Route::post('create', 'Controller@create')->middleware('user.status');
});

Route::group([
    'as' => 'like.',
    'namespace' => 'Api\Like',
    'prefix' => 'like',
	'middleware' => 'jwt.verify'    
], function () {
    Route::put('rate', 'Controller@rate')->middleware('user.status');
});

