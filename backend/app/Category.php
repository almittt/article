<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Get the category record associated with the article.
     */

    public function article()
    {
        return $this->hasOne('App\Article', 'category_id', 'id');
    }        
}
