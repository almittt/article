<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    const MODERATED = 'moderated';
    const ACCEPTED = 'accepted';
    const REJECTED = 'rejected';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * Get the user record associated with the comment.
     */

    public function user()
    {
        return $this->hasOne('App\User',  'id', 'user_id');
    }

    /**
     * Get the article record associated with the comment.
     */

    public function article()
    {
        return $this->hasOne('App\Article',  'id', 'article_id');
    }
}
