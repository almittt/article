<?php

namespace App\Entities\Category\Dto;


class UpdateCategoryDto
{
	public $id;
	public $name;
	
	function __construct(string $id, string $name)
	{
		$this->id = $id;
		$this->name = $name;
	}
}                