<?php

namespace App\Entities\Category\Dto;


class CreateCategoryDto
{
	public $name;
	
	function __construct(string $name)
	{
		$this->name = $name;
	}
}                