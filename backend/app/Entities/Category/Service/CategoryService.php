<?php

namespace App\Entities\Category\Service;

use App\Entities\Category\Repository\CategoryRepository;
use App\Entities\Category\Dto\CreateCategoryDto;
use App\Entities\Category\Dto\UpdateCategoryDto;
use App\Entities\User\Repository\UserRepository;


class CategoryService
{
	private $categoryRepository;
	private $userRepository; 

	function __construct(CategoryRepository $categoryRepository, UserRepository $userRepository)
	{
		$this->categoryRepository = $categoryRepository;
		$this->userRepository = $userRepository;
	}

	public function displayCategories()
	{
		return $this->categoryRepository->findAll();
	}

	public function displayCategory(string $id)
	{
		return $this->categoryRepository->findById($id);
	}

	public function createCategory(CreateCategoryDto $dto)
	{
		return $this->categoryRepository->setCategory($dto);
	}

	public function updateCategory(UpdateCategoryDto $dto)
	{
		return $this->categoryRepository->updateCategory($dto);
	}

	public function destroyCategory(string $id)
	{
		$moderator = $this->userRepository->getModeratorRole();

		if (auth()->user()->role === $moderator)
		{
			$category = $this->categoryRepository->findById($id);

			if(!$category->article) 
			{
				$this->categoryRepository->deleteCategory($id);

				return true;
			}

			return false;
		}
	}



}