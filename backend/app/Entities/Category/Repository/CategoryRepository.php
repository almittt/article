<?php

namespace App\Entities\Category\Repository;

use App\Category;
use App\Entities\Category\Dto\CreateCategoryDto;
use App\Entities\Category\Dto\UpdateCategoryDto;

class CategoryRepository
{
	private $categoryModel;
  	
 	function __construct(Category $categoryModel)
 	{
 		$this->categoryModel = $categoryModel;
 	}

 	public function findAll() {

 	  return $this->categoryModel::all();
 	}

 	public function findById(string $id)
    {

 	  return $this->categoryModel::find($id);
 	}

 	public function setCategory(CreateCategoryDto $dto)
 	{

 	  $category = new $this->categoryModel;

 	  $category->name = $dto->name;

 	  $category->save();
 	}

 	public function updateCategory(UpdateCategoryDto $dto)
 	{

 	  $category = $this->categoryModel::find($dto->id);

 	  $category->name = $dto->name;

 	  $category->save();
 	}

 	public function deleteCategory(string $id)
 	{
 		$category = Category::find($id);

 		$category->delete();
 	}

 } 