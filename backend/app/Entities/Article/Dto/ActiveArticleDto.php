<?php

namespace App\Entities\Article\Dto;


class ActiveArticleDto
{
	public $id;
	public $note;
	
	function __construct(string $id, string $note = null)
	{
		$this->id = $id;		
		$this->note = $note;
	}
}                