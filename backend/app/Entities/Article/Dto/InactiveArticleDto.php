<?php

namespace App\Entities\Article\Dto;


class InactiveArticleDto
{
	public $id;
	public $reason;
	
	function __construct(string $id, string $reason)
	{
		$this->id = $id;
		$this->reason = $reason;
	}
}                