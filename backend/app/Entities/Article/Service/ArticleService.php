<?php

namespace App\Entities\Article\Service;

use App\Entities\Article\Repository\ArticleRepository;

class ArticleService
{
	private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

	public function filter(string $category = null)
	{
		if ($category) {
			return $this->articleRepository->getArticlesByCategory($category);
		} else {
			return $this->articleRepository->getAllArticles();
		}
	}

	public function displayArticle(string $id)
	{
        return $this->articleRepository->getArticleById($id);
	}

    public function activateArticle(string $id) 
    {
        return $this->articleRepository->updateArticleStatusToActive($id); 
    }

    public function inactivateArticle(string $id) 
    {
        return $this->articleRepository->updateArticleStatusToInactive($id); 
    }

    public function resetArticleRate(string $id) 
    {
        $this->articleRepository->updateArticleRate($id);
        $this->articleRepository->deleteLikesByArticle($id); 
    }

    public function destroyArticle(string $id) 
    {
        $this->articleRepository->deleteLikesByArticle($id);
        $this->articleRepository->deleteCommentsByArticle($id);
        $this->articleRepository->deleteArticle($id); 
    }

}