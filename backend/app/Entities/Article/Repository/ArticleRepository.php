<?php

namespace App\Entities\Article\Repository;

use App\Article;
use App\Comment;
use App\Like;

class ArticleRepository
{

    public function getArticlesByCategory(string $category = null)
    {
        return Article::where('category_id', '=', $category)->paginate(15);
    }
    
	public function getAllArticles()
	{
		return Article::all();
	}

    public function getArticleById(string $article)
    {
        return Article::find($article);
    }


    public function getUserByArticleRelation(string $article)
    {
        return Article::find($article)->user;
    }

    public function updateArticleStatusToActive(string $id)
    {
        $status = Article::find($id);
        $status->status = Article::ACTIVE;
        $status->save();
    }

    public function updateArticleStatusToInactive(string $id)
    {
        $status = Article::find($id);
        $status->status = Article::INACTIVE;
        $status->save();
    }

    public function updateArticleRate(string $id)
    {
        $rate = Article::find($id);
        $rate->rating = 0;
        $rate->save();
    }

    public function deleteArticle(string $id)
    {
        Article::destroy($id);
    }


    public function deleteLikesByArticle(string $id)
    {
        Like::where('article_id', $id)->delete();
    }

    public function deleteCommentsByArticle(string $id)
    {
        Comment::where('article_id', $id)->delete();
    }


}