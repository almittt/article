<?php

namespace App\Entities\User\Dto;


class InactiveUserDto
{
	public $id;
	public $reason;
	
	function __construct(string $id, string $reason)
	{
		$this->id = $id;
		$this->reason = $reason;
	}
}                