<?php

namespace App\Entities\User\Dto;


class ActiveUserDto
{
	public $id;
	public $note;
	
	function __construct(string $id, string $note = null)
	{
		$this->id = $id;		
		$this->note = $note;
	}
}                