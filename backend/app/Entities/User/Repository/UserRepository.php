<?php

namespace App\Entities\User\Repository;

use App\Article;
use App\Comment;
use App\Like;
use App\User;

class UserRepository
{
    private $userModel;

    function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    public function findUsersByRole(string $role = null)
    {
        return User::where('role', '=', $role)->paginate(15);
    }

	public function findAllUsers()
	{
		return User::paginate(15);
	}

    public function updateUserStatusToActive($id)
    {
        $status = User::find($id);
        $status->status = User::ACTIVE;
        $status->save();
    }

    public function updateUserStatusToInactive($id)
    {
        $status = User::find($id);
        $status->status = User::INACTIVE;
        $status->save();
    }

    public function getArticlesByUserRelation(string $user)
    {
        return User::find($user)->articles;
    }

    public function getUserById(string $user)
    {
        return User::find($user);
    }

    public function getModeratorRole() {
        
        return User::MODERATOR;
    }

    public function findAuthorsAndUsersRoles()
    {
        return [
            User::AUTHOR  => 'Авторы',
            User::USER  => 'Пользователи'
        ];        
    }

    public function findAllUsersAndAuthors()
    {
        return User::where('role', User::AUTHOR)->orWhere('role', User::USER)->paginate(15);
    }

    public function deleteUser                                                                                                                                   (string $id)
    {
        User::destroy($id);
    }


    public function deleteLikesByUser(string $id)
    {
        Like::where('user_id', $id)->delete();
    }

    public function deleteCommentsByUser(string $id)
    {
        Comment::where('user_id', $id)->delete();
    }

    public function deleteArticlesByUser(string $id)
    {
        Article::where('user_id', $id)->delete();
    }
}