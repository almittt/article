<?php

namespace App\Entities\User\Service;

use App\Entities\User\Repository\UserRepository;

class UserService
{
	private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

	public function filter(string $role = null)
	{
		if ($role) {
			return $this->userRepository->findUsersByRole($role);
		} else {
			return $this->userRepository->findAllUsersAndAuthors();
		}
	}

    public function displayRolesFilter()
    {
        return $this->userRepository->findAuthorsAndUsersRoles();
    }

    public function displayUser(string $id)
    {
        return $this->userRepository->getUserById($id);
    }

    public function activateUser(string $id) 
    {
        return $this->userRepository->updateUserStatusToActive($id); 
    }

    public function inactivateUser(string $id) 
    {
        return $this->userRepository->updateUserStatusToInactive($id); 
    }

    public function destroyUser(string $id) 
    {
        $this->userRepository->deleteArticlesByUser($id);
        $this->userRepository->deleteCommentsByUser($id);
        $this->userRepository->deleteLikesByUser($id);
        $this->userRepository->deleteUser($id); 
    }
}