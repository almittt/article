<?php

namespace App\Entities\Comment\Service;

use App\Entities\Comment\Repository\CommentRepository;

class CommentService
{
	private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function displayComments() 
    {
        return $this->commentRepository->findAllCommentsSortByDate(); 
    }

    public function displayComment(string $id) 
    {
        return $this->commentRepository->findCommentById($id); 
    }

    public function acceptComment(string $id) 
    {
        return $this->commentRepository->updateCommentStatusToAccept($id); 
    }

    public function rejectComment(string $id) 
    {
        return $this->commentRepository->updateCommentStatusToReject($id); 
    }

	// public function filter(string $article = null, string $user = null)
	// {
	// 	if ($article) {
	// 		return $this->commentRepository->getCommentsByArticle($article);
	// 	}

 //        if ($user) {
 //            return $this->commentRepository->getCommentsByUser($user);
 //        }

 //        return $this->commentRepository->getAllComments();
	// }
}