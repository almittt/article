<?php

namespace App\Entities\Comment\Dto;


class AcceptCommentDto
{
	public $id;
	public $note;
	
	function __construct(string $id, string $note = null)
	{
		$this->id = $id;		
		$this->note = $note;
	}
}                