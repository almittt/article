<?php

namespace App\Entities\Comment\Dto;


class RejectCommentDto
{
	public $id;
	public $reason;
	
	function __construct(string $id, string $reason)
	{
		$this->id = $id;
		$this->reason = $reason;
	}
}                