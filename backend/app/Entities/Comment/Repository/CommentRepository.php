<?php

namespace App\Entities\Comment\Repository;

use App\Comment;
use App\User;
use App\Article;

class CommentRepository
{
    private $modelUser;
    private $modelArticle;

    public function __construct(User $modelUser, Article $modelArticle)
    {
        $this->modelUser = $modelUser;
        $this->modelArticle = $modelArticle;
    }

    public function getCommentsByArticle(string $article = null)
    {
        return $this->modelArticle::find($article)->comments;
    }

    public function getCommentsByUser(string $user = null)
    {
        return $this->modelUser::find($user)->comments->paginate(15);
    }

    public function findCommentById($id)
    {
        return Comment::find($id);
    }
    
	public function findAllCommentsSortByDate()
	{
		return Comment::paginate(15)->sortByDesc('created_at');
	}

    public function updateCommentStatusToAccept($id)
    {
        $status = Comment::find($id);
        $status->status = Comment::ACCEPTED;
        $status->save();
    }

    public function updateCommentStatusToReject($id)
    {
        $status = Comment::find($id);
        $status->status = Comment::REJECTED;
        $status->save();
    }


    public function getUserByCommentRelation(string $article)
    {
        return Comment::find($article)->user;
    }

    public function getArticleByCommentRelation(string $id)
    {
        return Comment::find($id)->article;
    }

    public function updateComment(string $id, string $text)
    {
        $comment = Comment::find($id);

        $comment->text = $text;
        $comment->save();
    }
}