<?php

namespace App\Services\Admin\Comment;

use App\Repositories\Admin\Comment\CommentRepository;

class CommentService
{
    /**
     * Repository variable.
     *
     * @var App\Repositories\Admin\Comment\CommentRepository
     */
	private $commentRepository;


    /**
     * Repository variable.
     *
     * @param  App\Repositories\Admin\Comment\CommentRepository  $commentRepository
     */
    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * Filter articles by category
     *
     * @param  string  $role
     * @return Illuminate\Pagination\LengthAwarePaginator
    */
	public function filter(string $article = null, string $user = null)
	{
		if ($article) {
			return $this->commentRepository->getCommentsByArticle($article);
		}

        if ($user) {
            return $this->commentRepository->getCommentsByUser($user);
        }

        return $this->commentRepository->getAllComments();
	}
}