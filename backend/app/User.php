<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const ADMIN = 'admin';
    const MODERATOR = 'moderator';
    const AUTHOR = 'author';    
    const USER = 'user';

    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const MODERATED = 'moderated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'password', 'login', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
    */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
      * Return a key value array, containing any custom claims to be added to the JWT.
      *
      * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get user articles.
     */

    public function articles()
    {
        return $this->hasMany('App\Article', 'user_id', 'id');
    } 

    /**
     * Get user likes.
     */

    public function likes()
    {
        return $this->hasMany('App\Like', 'user_id', 'id');
    }

    /**
     * Get user comments.
     */

    public function comments()
    {
        return $this->hasMany('App\Comment', 'user_id', 'id');
    }


    /**
     * Get roles filter.
     *
     * @return array
     */

    public function getRolesFilter()
    {
        return [
            self::ADMIN   => 'Админиcтраторы',
            self::MODERATOR => 'Модераторы',
            self::AUTHOR  => 'Авторы',
            self::USER  => 'Пользователи'
        ];        
    }

}
