<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const MODERATED = 'moderated';
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'articles';


    /**
     * Get the category record associated with the article.
     */

    public function category()
    {
        return $this->hasOne('App\Category',  'id', 'category_id');
    }

    /**
     * Get the user record associated with the article.
     */

    public function user()
    {
        return $this->hasOne('App\User',  'id', 'user_id');
    }

    /**
     * Get comments on the article.
     */

    public function comments()
    {
        return $this->hasMany('App\Comment', 'article_id', 'id');
    }

    // /**
    //  * The roles that belong to the user.
    //  */
    // public function too()
    // {
    //     return $this->belongsToMany('App\User');
    // }    
}
