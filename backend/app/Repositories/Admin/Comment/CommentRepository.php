<?php

namespace App\Repositories\Admin\Comment;

use App\Comment;
use App\User;
use App\Article;

class CommentRepository
{
    /**
     * user model variable.
     *
     * @var App\User
     */
    private $modelUser;

    /**
     * article model variable.
     *
     * @var App\Article
     */
    private $modelArticle;



    /**
     * Construct.
     *
     * @param  App\User $modelUser
     * @param  App\Article  $modelArticle
     */
    public function __construct(User $modelUser, Article $modelArticle)
    {
        $this->modelUser = $modelUser;
        $this->modelArticle = $modelArticle;
    }


    /**
     * Display comments by article
     *
     * @param  string  $article
     * @return Illuminate\Database\Eloquent\Collection
    */
    public function getCommentsByArticle(string $article = null)
    {
        return $this->modelArticle::find($article)->comments;
    }

    /**
     * Display comments by user
     *
     * @param  string  $category
     * @return Illuminate\Pagination\LengthAwarePaginator
    */
    public function getCommentsByUser(string $user = null)
    {
        return $this->modelUser::find($user)->comments->paginate(15);
    }

    
    /**
     * Display all comments
     *
     * @return App\Comment
    */
	public function getAllComments()
	{
		return Comment::paginate(15);
	}

    /**
     * Get the user record associated with the comment.
     *
     * @param string $article
     * @return App\Comment
    */
    public function getUserByCommentRelation(string $article)
    {
        return Comment::find($article)->user;
    }

    /**
     * Get the article record associated with the comment.
     *
     * @param string $id
     * @return App\Comment
    */
    public function getArticleByCommentRelation(string $id)
    {
        return Comment::find($id)->article;
    }


    /**
     * Comment text update.
     *
     * @param string $id
     * @param string $text
    */
    public function updateComment(string $id, string $text)
    {
        $comment = Comment::find($id);

        $comment->text = $text;
        $comment->save();
    }



}