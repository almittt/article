<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;


class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new administrator';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('Enter your name');
        $surname = $this->ask('Enter your surname');
        $email = $this->ask('Enter your email');
        $login = $this->ask('Enter your login');
        $password = $this->secret('Enter password');
        $passwordConfirmation = $this->secret('Confirm password');

        $validator = $this->validate($name, $surname, $email, $login, $password, $passwordConfirmation);
        if ($validator->fails()) {
            $this->info('Unable to create admin!');
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }
        $this->create($name, $surname, $email, $login, $password);
        $this->info('Admin created successfully!');
    }

    /**
     * Data validation function.
     *
     * @param  string $name
     * @param  string $surname
     * @param  string $email
     * @param  string $login
     * @param  string $password
     * @param  string $passwordConfirmation
     * @return \Facades\Validator
     */
    protected function validate(string $name, string $surname, string $email, string $login, string $password, string $passwordConfirmation)
    {
        return \Validator::make([
            'name' => utf8_encode($name),
            'surname' => utf8_encode($surname),
            'email' => $email,
            'login' => $login ,
            'password' => $password,
            'password_confirmation' => $passwordConfirmation,
        ], $this->rules());
    }

    /**
     * Check validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'name' => [
                'required', 'string', 'min:2', 'max:40',
            ],
            'surname' => [
                'required', 'string', 'min:2', 'max:40',            ],
            'email' => [
                'required','email', 'string', 'max:255', 'unique:users,email',
            ],
            'login' => [
                'required', 'string', 'min:6', 'max:30', 'unique:users,login', 'regex:/(^([a-zA-Z]+)(\d+)?$)/u', 
            ],
            'password' => 'required|string|min:8|max:36|confirmed|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        ];
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  string $name
     * @param  string $surname
     * @param  string $email
     * @param  string $login
     * @param  string $password
     * @return \App\User
     */
    protected function create(string $name, string $surname, string $email, string $login, string $password)
    {
        return User::create([
            'name' => $name,
            'surname' => $surname,
            'login' => $login,
            'email' => $email,
            'password' => Hash::make($password),
            'role' => User::ADMIN,
        ]);
    }

}
