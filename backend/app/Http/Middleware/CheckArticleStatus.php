<?php

namespace App\Http\Middleware;

use Closure;
use App\Article;
use App\Entities\Article\Service\ArticleService;


class CheckArticleStatus
{
    /**
     * @var Article
     */
    private $model;

    /**
     * @var ArticleService
     */
    private $service;

    /**
     * CheckArticleStatus constructor.
     * @param Article $model
     * @param ArticleService $service
     */
    public function __construct(Article $model, ArticleService $service)
    {
        $this->model = $model;
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  \Article  $model
     * @param  \ArticleService  $service
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $article = $this->service->displayArticle($request->id);

        if ($article->status === $this->model::MODERATED)
        {
            return response()->json([
                'status' => 'error',
                'application' => 'Article on moderation!'
            ], 403);
        }

        if ($article->status === $this->model::INACTIVE)
        {
            return response()->json([
                'status' => 'error',
                'application' => 'Article banned!'
            ], 403);
        }

        return $next($request);
    }
}
