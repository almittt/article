<?php

namespace App\Http\Middleware;

use Closure;
use App\Comment;
use App\Entities\Comment\Service\CommentService;


class CheckCommentStatus
{
    /**
     * @var Comment
     */
    private $model;

    /**
     * @var CommentService
     */
    private $service;

    /**
     * CheckCommentStatus constructor.
     * @param Comment $model
     * @param CommentService $service
     */
    public function __construct(Comment $model, CommentService $service)
    {
        $this->model = $model;
        $this->service = $service;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  \Comment  $model
     * @param  \CommentService  $service
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $comment = $this->service->displayComment($request->id);

        if ($comment->status === $this->model::MODERATED)
        {
            return response()->json([
                'status' => 'error',
                'application' => 'Сomment on moderation!'
            ], 403);
        }

        if ($comment->status === $this->model::REJECTED)
        {
            return response()->json([
                'status' => 'error',
                'application' => 'Сomment rejected!'
            ], 403);
        }
        return $next($request);
    }
}
