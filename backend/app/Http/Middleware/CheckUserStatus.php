<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckUserStatus
{
    /**
     * @var User
     */
    private $model;

    /**
     * @var UserService
     */
    private $service;

    /**
     * CheckUserStatus constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  \User  $model
     * @param  \UserService  $service
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->status === $this->model::INACTIVE) {
            return response()->json([
                'status' => 'error',
                'application' => 'User banned!'
            ], 403);
        }

        if (auth()->user()->status === $this->model::MODERATED) {
            return response()->json([
                'status' => 'error',
                'application' => 'User on moderation!'
            ], 403);
        }

        return $next($request);
    }
}
