<?php

namespace App\Http\Middleware;

use Closure;

class GuardSwitcher
{
    /**
     * Changes default guard from "api" to "web"
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->getDefaultDriver() === 'api') {
            auth()->setDefaultDriver('web');
        }
        return $next($request);        
    }
}
