<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckRoleInAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((auth()->user()->role === User::ADMIN) || (auth()->user()->role === User::MODERATOR)) {
            return $next($request);

        }

        return abort(403);
    }
}
