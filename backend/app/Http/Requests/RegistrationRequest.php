<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users,email',
            'login' => 'required|string|min:6|max:30|unique:users,login|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'password' => 'required|string|min:8|max:36|confirmed|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'name' => 'required|string|min:2|max:40',
            'surname' => 'required|string|min:2|max:40',
            'role' => [ 'required', 'string', Rule::in(['author', 'user']) ],
        ];                          
    }
}
