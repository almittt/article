<?php

namespace App\Http\Requests\Admin;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:40',
            'surname' => 'required|string|min:2|max:40',
            'login' => 'required|string|min:6|max:30|unique:users,login|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:8|max:36|confirmed|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        ];
    }
}
