<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ArticleActiveRequest;
use App\Http\Requests\Admin\ArticleInactiveRequest;
use App\Entities\Article\Dto\ActiveArticleDto;
use App\Entities\Article\Dto\InactiveArticleDto;
use App\Entities\Article\Service\ArticleService;
use App\Entities\Category\Service\CategoryService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $request;
    private $articleService;
    private $categoryService;

    function __construct(
        Request $request,
        ArticleService $articleService,
        CategoryService $categoryService
    )
    {
        $this->request = $request;
        $this->articleService = $articleService;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $articles = $this->articleService->filter($this->request->category);
        $categories = $this->categoryService->displayCategories();
        return view('articles.index', [
            'articles' => $articles,
            'categories' => $categories
        ]);
    
    }

    public function show()
    {
        $article = $this->articleService->displayArticle($this->request->article);
        return view('articles.show', [
            'article' => $article
        ]);
    }

    public function active(string $id, ArticleActiveRequest $request)
    {
        $dto = new ActiveArticleDto($id, $request->note);

        $this->articleService->activateArticle($this->request->id);
        return redirect()->route('article.index')->withStatus(__('Статья активирована!'));
    }

    public function inactive(string $id, ArticleInactiveRequest $request)
    {
        $dto = new InactiveArticleDto($id, $request->reason);

        $this->articleService->inactivateArticle($this->request->id);
        return redirect()->route('article.index')->withStatus(__('Статья деактивирована!'));
    }

    public function reset()
    {
        $this->articleService->resetArticleRate($this->request->id);                
        return redirect()-> back()->withStatus(__('Рейтинг обнулен!'));
    }

    public function destroy()
    {
        $this->articleService->destroyArticle($this->request->id);                
        return redirect()-> back()->withStatus(__('Статья удалена!'));
    }

}
