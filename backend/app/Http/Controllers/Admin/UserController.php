<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserActiveRequest;
use App\Http\Requests\Admin\UserInactiveRequest;
use App\Entities\User\Dto\ActiveUserDto;
use App\Entities\User\Dto\InactiveUserDto;
use App\Entities\User\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userService;
    private $request;

    function __construct(UserService $userService, Request $request)
    {
        $this->userService = $userService;
        $this->request = $request;
    } 

    public function index(User $userModel)
    {
        $roles = $this->userService->displayRolesFilter();
        $users = $this->userService->filter($this->request->role);
        return view('users.index', [
            'users' => $users,
            'roles' => $roles
        ]);
    }

    public function show()
    {
        $user = $this->userService->displayUser($this->request->id);

        return view('users.show', [
            'user'  => $user
        ]);
    }

    public function active(string $id, UserActiveRequest $request)
    {
        $dto = new ActiveUserDto($id, $request->note);

        $this->userService->activateUser($this->request->id);
        return redirect()->route('user.index')->withStatus(__('Пользователь активирован!'));
    }

    public function inactive(string $id, UserInactiveRequest $request)
    {
        $dto = new InactiveUserDto($id, $request->reason);

        $this->userService->inactivateUser($this->request->id);
        return redirect()->route('user.index')->withStatus(__('Пользователь деактивирован!'));
    }

    public function create()
    {
        if (auth()->user()->role === 'admin') {
            return view('users.create');               
        }                                     
        return abort(403);
    }                                                          

    public function store(UserRequest $request, User $model)
    {
        $model->create([
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'login' => $request->login,
            'password' => Hash::make($request->password),
            'role' => $model::MODERATOR,

        ]);

        return redirect()->route('user.index')->withStatus(__('Модератор успешно создан.'));
    }

    public function edit(User $user)
    {
        if ($user->role == User::ADMIN) {
            return redirect()->route('user.index')->withStatus(__('Вы не можете редактировать администратора.'));
        }

        return view('users.edit', compact('user'));
    }

    public function update(UserRequest $request, User  $user)
    {
        $hasPassword = $request->get('password');
        $user->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except([$hasPassword ? '' : 'password']
        ));

        return redirect()->route('user.index')->withStatus(__('Пользователь успешно обновлен.'));
    }

    public function destroy(User  $user)
    {

        if (auth()->user()->role !== $user::ADMIN) {
            return redirect()->route('user.index')->withStatus(__('У вас нет прав на это действие!'));
        }

        $this->userService->destroyUser($user->id);

        return redirect()->route('user.index')->withStatus(__('Пользователь успешно удален.'));
    }
}
