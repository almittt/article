<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category\Service\CategoryService;
use App\Entities\Category\Dto\CreateCategoryDto;
use App\Entities\Category\Dto\UpdateCategoryDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryCreateRequest;
use App\Http\Requests\Admin\CategoryUpdateRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	private $categoryService;
	private $request;

	function __construct(CategoryService $categoryService, Request $request) {
		$this->categoryService = $categoryService;
		$this->request = $request;
	}

	public function index()
	{
		$this->categoryService->displayCategories();

	    return view('categories.index', [
	    	'categories' => $this->categoryService->displayCategories(),
	    ]);
	}

	public function create()
	{
	    return view('categories.create');
	}

	public function store(CategoryCreateRequest $request)
	{
		$dto = new CreateCategoryDto($request->name);

		$this->categoryService->createCategory($dto);

		return redirect()->route('category.index')->with('status', 'Категория успешно добавлена!');
	}

	public function edit()
	{
	    return view('categories.edit', [
	    	'category' => $this->categoryService->displayCategory($this->request->id),
	    ]);
	}

	public function update(string $id, CategoryUpdateRequest $request)
	{
		$dto = new UpdateCategoryDto($id, $request->name);

		$this->categoryService->updateCategory($dto);

		return redirect()->route('category.index')->with('status', 'Категория успешно обновлена!');
	}

	public function destroy(string $id)
	{

		if($this->categoryService->destroyCategory($id))
		{
    		return redirect()->route('category.index')->with('status', 'Категория успешно удалена!');
		}

 		return redirect()->route('category.index')->with('status', 'По данной категории есть статьи!');


	}


}
