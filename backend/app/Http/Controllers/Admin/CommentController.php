<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CommentRequest;
use App\Http\Requests\Admin\CommentAcceptRequest;
use App\Http\Requests\Admin\CommentRejectRequest;
use App\Entities\Comment\Service\CommentService;
use App\Entities\Comment\Dto\AcceptCommentDto;  
use App\Entities\Comment\Dto\RejectCommentDto;  
use Illuminate\Http\Request;

class CommentController extends Controller
{
    private $commentService;
    private $request;

    function __construct(
        CommentService $commentService,
        Request $request
    )
    {
        $this->commentService = $commentService;
        $this->request = $request; 
    }

    public function index()
    {
        $comments = $this->commentService->displayComments();
        return view('comments.index', [
            'comments' => $comments
        ]);

    }

    public function show()
    {
        $comment = $this->commentService->displayComment($this->request->id);
        $comment = $this->commentService->displayComment($this->request->id);
        return view('comments.show', [
            'comment' => $comment
        ]);
    }

    public function accept(string $id, CommentAcceptRequest $request)
    {
        $dto = new AcceptCommentDto($id, $request->note);

        $comment = $this->commentService->acceptComment($this->request->id);
        return redirect()->route('comment.index')->withStatus(__('Комментарий принят!'));
    }

    public function reject(string $id, CommentRejectRequest $request)
    {
        $dto = new RejectCommentDto($id, $request->reason);

        $comment = $this->commentService->rejectComment($this->request->id);
        return redirect()->route('comment.index')->withStatus(__('Комментарий отклонен!'));
    }

    // /**
    //  * Remove the specified user from storage
    //  *
    //  * @param  \App\User  $user
    //  * @return \Illuminate\Http\RedirectResponse
    //  */
    // public function destroy(User  $user)
    // {

    //     if ($user->role === $user::ADMIN) {
    //         return redirect()->route('user.index')->withStatus(__('Невозможно удалить администратора.'));
    //     }

    //     $user->delete();

    //     return redirect()->route('user.index')->withStatus(__('Пользователь успешно удален.'));
    // }
    
}
