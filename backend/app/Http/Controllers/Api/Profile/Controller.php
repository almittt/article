<?php

namespace App\Http\Controllers\Api\Profile;

use App\User;
use App\Http\Controllers\Controller as  BaseController;
use App\Http\Requests\Profile\UpdateRequest;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    /**
     * Show profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function show()
    {
    	$profile = User::find(auth()->user()->id);

		return response()->json([
			'id' => $profile->id,
			'name' => $profile->name,
			'surname' => $profile->surname,
			'role' => $profile->role
		]);     	
    }

    /**
     * Update profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update(UpdateRequest $request)
    {
    	$profile = User::find(auth()->user()->id);

    	$profile->name = request('name');
    	$profile->surname = request('surname');
    	$profile->role = request('role');
    	$profile->save();

		return response()->json([
            'status' => 'success',
            'message' => 'Profile updated!'
        ]);     	
    }                                                           

}
