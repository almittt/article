<?php

namespace App\Http\Controllers\Api\Article;

use App\Article;
use App\User;
use App\Like;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests\Article\CreateRequest;
use App\Http\Requests\Article\UpdateRequest;
use App\Http\Requests\Article\DeleteRequest;
use App\Http\Requests\Article\RateRequest;
use Illuminate\Http\Request;

class Controller extends BaseController 
{

    /**
    * Show my articles.
    *
    * @return \Illuminate\Http\JsonResponse
    */

    public function my()
    {
    	$user = User::find(auth()->user()->id);

		return response()->json([
			'articles' => $user->articles->all(),
		]);     	
    }


    /**
    * Show article.
    *
    * @return \Illuminate\Http\JsonResponse
    */

    public function show($id)
    {
    	$article = Article::find($id);

		return response()->json([
            'status' => 'success',            
			'id' => $article->id,
			'name' => $article->name,
			'text' => $article->text,
			'preview' => public_path().$article->preview,
			'rating' => $article->rating,
			'category' => $article->category->name,
			'user_name' => $article->user->name,
			'user_surname' => $article->user->surname,
		]);     	
    }

    /**
     * Create article.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function create(CreateRequest $request)
    {
    	$user = User::find(auth()->user()->id);

    	if($user->role !== User::AUTHOR) {
    		return response()->json(['message' => 'Only authors can create articles!']);
    	}

    	$previewName = time().'.'.request()->preview->getClientOriginalExtension();

    	request()->preview->move(public_path('images'), $previewName);

    	$article = new Article();

    	$article->name = $request->name;
    	$article->text = $request->text;
    	$article->category_id = $request->category;
    	$article->preview = '/'.$previewName;
    	$article->user_id = $user->id;
    	$article->rating = 0;
        $article->status = Article::MODERATED;
    	$article->save();  

		return response()->json([
            'status' => 'success',
            'message' => 'Article successfully created!'
        ]);     	
    }

    /**
     * Update article.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update(UpdateRequest $request)
    {


    	$user = User::find(auth()->user()->id);

    	if($user->role !== User::AUTHOR) {
    		return response()->json([
                'status' => 'success',
                'message' => 'Only authors can update articles!'
            ]);
    	}

    	$article = Article::find($request->id);

    	unlink(public_path().$article->preview);
    
    	$previewName = time().'.'.request()->preview->getClientOriginalExtension();

    	request()->preview->move(public_path('images'), $previewName);

    	$article = Article::find($request->id);

    	$article->name = $request->name;
    	$article->text = $request->text;
    	$article->category_id = $request->category;
    	$article->preview = '/'.$previewName;
    	$article->save();  

		return response()->json([
            'status' => 'success',
            'message' => 'Article updated successfully!'
        ]);     	
    }

    /**
     * Delete article.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function delete(DeleteRequest $request)
    {
    	$user = User::find(auth()->user()->id);

    	if($user->role !== User::AUTHOR) {
    		return response()->json(['message' => 'Only authors can delete articles!']);
    	}

    	$article = Article::find($request->id);

    	$article->delete();

		return response()->json([
            'status' => 'success',
            'message' => 'Article successfully deleted!'
        ]);     	
    }
}
