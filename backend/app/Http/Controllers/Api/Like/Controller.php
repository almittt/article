<?php

namespace App\Http\Controllers\Api\Like;

use App\User;
use App\Like;
use App\Article;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests\Like\RateRequest;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    /**
     * Rate article.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function rate(RateRequest $request)
    {
    	$user = User::find(auth()->user()->id);

    	$like = $user->likes()
    		->where('user_id', $user->id)
    		->where('article_id', $request->article_id)
    		->get();


    	if(!$like->isEmpty()) {
    		return response()->json(['message' => 'User has already liked this article!']);
    	}

    	$article = Article::find($request->article_id);

    	$article->rating = $article->rating + 1;
    	$article->save();

    	$like = new Like();

    	$like->user_id = $user->id;
    	$like->article_id = $article->id;
    	$like->save();                   

		return response()->json([
            'status' => 'success',            
            'message' => 'Article successfully liked!'
        ]);     	
    }
    
}
