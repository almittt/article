<?php

namespace App\Http\Controllers\Api\Comment;

use App\Comment;
use App\Article;
use App\User;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests\Comment\CreateRequest;
use Illuminate\Http\Request;

class Controller extends BaseController
{

    /**
    * Show article comments.
    *
    * @return \Illuminate\Http\JsonResponse
    */

    public function show($id)
    {
    	$article = Article::find($id);

    	$comments = $article->comments;

		return response()->json([
			'comments' => $article->comments
		]);     	
    }

    /**
     * Create comment.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function create(CreateRequest $request)
    {
    	$user = User::find(auth()->user()->id);

    	$comment = new Comment();

    	$comment->text = $request->text;
    	$comment->article_id = $request->article_id;
    	$comment->user_id = $user->id;
        $comment->status = Comment::MODERATED;
    	$comment->save();  

		return response()->json([
            'status' => 'success',
            'message' => 'Comment sent for moderation!'
        ]);     	
    }
    
}
